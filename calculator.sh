#!/usr/bin/env bash


a=${1}
i=${2}
b=${3}

function add() {
	echo "$((a + b))"
} 
function substract() {
	echo "$((a - b))"
} 
function multiply() {
	echo "$((a * b))"
}
function divide() {
	echo "$((a / b))"
}

case ${i} in
	"+")
		add
		;;


	"-")
		substract
		;;

	".")
		multiply
		;;
		
	"/")
		divide
		;;
	*)
		echo "dodawanie: + , odejmowanie: - , mnożenie: . , dzielenie /"	;;

esac
